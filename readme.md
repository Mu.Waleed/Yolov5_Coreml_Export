This repo will help you to convert your yolov5 model trained weights to mlmodel.

Note: This repos uses [coreml-tools](https://github.com/dbsystel/yolov5-coreml-tools) and [yolov5 version 4](https://github.com/ultralytics/yolov5/tree/v4.0) for conversion.

```
git clone https://gitlab.com/Mu.Waleed/Yolov5_Coreml_Export
cd yolov5-coreml-tools
cd yolov5-coreml-tools
poetry install
```
```
poetry run coreml-export
```
Note: There is [conda.yaml](https://gitlab.com/Mu.Waleed/Yolov5_Coreml_Export/-/blob/main/conda.yaml) for the exact module version I use in the environment. You can use same version if you face any issues.
